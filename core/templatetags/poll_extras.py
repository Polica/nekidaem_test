from django import template

from core.models import User

register = template.Library()


@register.filter(name='get_user_by_blog')
def get_user_by_blog(blog):
    """Removes all values of arg from the given string"""
    return User.objects.get(blog_id=blog).username


@register.filter(name='inside')
def inside(value, arg):
    return value in arg
