from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect

from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import TemplateView, CreateView, ListView, DetailView

from core.models import Post, Blog, User, ReadHistory


class HomeView(TemplateView):
    model = Post
    template_name = 'home.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        posts = Post.objects.filter(blog__in=user.signed_blog.all()).order_by('-date')
        result = [post for post in posts if not post.is_user_read(user=user)]
        context['posts'] = result
        return context


class PostView(CreateView):
    model = Post
    fields = ['title', 'text']
    template_name = 'post.html'
    success_url = '/'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, *kwargs)

    def form_valid(self, form):
        blog = self.request.user.blog
        form.instance.blog = blog
        return super().form_valid(form)


class DetailPostView(DetailView):
    model = Post
    template_name = 'post_detail.html'


class BlogView(ListView):
    model = Blog
    template_name = 'blog_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['signed_blogs'] = self.request.user.signed_blog.values_list('pk', flat=True)
        return context


class SubscribeBlog(View):

    @staticmethod
    def get(request, pk):
        try:
            blog = get_object_or_404(Blog, pk=pk)
            user = request.user
            if blog not in user.signed_blog.all():
                """
                    Adding blog in signed Blog
                """
                user.signed_blog.add(blog)
                user.save()
            else:
                """
                   Removing blog from signed Blog add clearing read messages
                """
                ReadHistory.objects.filter(user=user, post__blog=blog).delete()
                user.signed_blog.remove(blog)
                user.save()
            return redirect('/')
        except Blog.DoesNotExist:
            raise Http404


class ReadPost(View):

    @staticmethod
    def get(request, pk):
        try:
            post = get_object_or_404(Post, pk=pk)
            user = request.user
            read_history = ReadHistory.objects.filter(post=post, user=user)
            if not read_history.exists():
                ReadHistory.objects.create(post=post, user=user)
                return redirect('/')
            else:
                raise Http404
        except Post.DoesNotExist or User.DoesNotExist:
            raise Http404
