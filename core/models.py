from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext as _

from core.managers import UserManager
from core.tasks import post_add

# TODO repair User password field


class Blog(models.Model):
    title = models.CharField(_('title'),
                             max_length=255)

    def __str__(self):
        return self.title


class Post(models.Model):
    title = models.CharField(_('title'),
                             max_length=255)
    text = models.TextField(_('text'))
    date = models.DateTimeField(_('date'),
                                auto_now_add=True)
    blog = models.ForeignKey(_('Blog'),
                             on_delete=models.PROTECT)

    def is_user_read(self, user):
        return ReadHistory.objects.filter(post_id=self.id, user=user).exists()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super().save()
        users = list(User.objects.filter(signed_blog__in=[self.blog]).values_list('email', flat=True))
        post_add.send(sender=self.__class__, pk=self.pk, users=users)

    def __str__(self):
        return self.title


class ReadHistory(models.Model):
    post = models.ForeignKey(_('Post'),
                             on_delete=models.CASCADE,
                             related_name='read_post')
    date = models.DateTimeField(_('date'),
                                auto_now_add=True),
    user = models.ForeignKey('User',
                             on_delete=models.CASCADE,
                             related_name='user_read')


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'),
                                max_length=100, unique=True)
    first_name = models.CharField(_('first name'),
                                  max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'),
                                       auto_now_add=True)
    is_staff = models.BooleanField(_('is_staff'),
                                   default=False)
    is_superuser = models.BooleanField(_('is_superuser'),
                                       default=False)
    email = models.EmailField(_('email'))
    blog = models.ForeignKey(_('Blog'),
                             on_delete=models.CASCADE,
                             related_name='user_blog',
                             blank=True,
                             null=True)
    signed_blog = models.ManyToManyField(_('Blog'),
                                         related_name='signed_blog',
                                         null=True,
                                         blank=True)
    objects = UserManager()

    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def delete(self, using=None, keep_parents=False):
        self.blog.delete()
        super().delete()

    def save(self, *args, **kwargs):
        if not self.blog_id:
            blog = Blog.objects.create(title=self.username + " blog")
            self.blog = blog
            self.save()
        super(User, self).save(*args, **kwargs)
