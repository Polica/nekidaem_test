from django.conf import settings
from django.core.mail import EmailMessage
from django.dispatch import Signal, receiver

from nekidaem_test.celery import app

post_add = Signal(providing_args=['pk', 'users'])

# TODO send message for all signed users


@app.task(name="sending_email")
def send_notification(pk, users):
    email = EmailMessage(
        'Уведомление о новом посте',
        'Ссылка на пост ' + str(pk),
        settings.EMAIL_HOST_USER,
        users,
    )
    email.send()


@receiver(post_add)
def post_save_receiver(sender, pk, users, **kwargs):
    print('asdasdasd')
    send_notification.delay(pk, users)
    print('qweqwe')


