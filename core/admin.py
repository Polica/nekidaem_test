from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import Group

from core.models import Post, User, Blog


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    fields = ('username', 'password', 'email', 'is_staff', 'is_superuser', 'signed_blog')
    readonly_fields = 'blog',


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    pass


@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    pass


admin.site.unregister(Group)
