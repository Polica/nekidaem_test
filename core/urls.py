from django.urls import path

from core.views import HomeView, PostView, BlogView, SubscribeBlog, ReadPost, DetailPostView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('post/', PostView.as_view(), name='post'),
    path('post/<int:pk>/', DetailPostView.as_view(), name='post-detail'),
    path('blogs/', BlogView.as_view(), name='blogs'),
    path('subscribe/<int:pk>/', SubscribeBlog.as_view(), name='subscribe-blog'),
    path('read/<int:pk>/', ReadPost.as_view(), name='read-post'),
]